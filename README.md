# SRL-scripts

This is a repository of scripts for Old School RuneScape. They require [Simba](https://github.com/MerlijnWajer/Simba), [SRL](https://github.com/SRL/SRL), and [RSWalker](https://github.com/ollydev/RSWalker).

If you lack these components, or don't know what they are, please refer to [this tutorial](https://villavu.com/forum/showthread.php?t=118223) to get up and running.

Documentation for each script is given in the source file itself. For help, please contact me at the [SRL Forums](https://villavu.com/forum/member.php?u=72074), on [IRC](https://villavu.com/forum/chat_irc.php), or on [Discord](https://discordapp.com/invite/627s6fm).

Identifiable information (account credentials and antiban tasks/breaks) is scrubbed from these scripts on every commit. This is for my safety as well as your own: if you are interested in adding antiban or breaking to your scripts, read [this documentation](https://srl.github.io/SRL/antiban.html) and generate something unique to you.

Here follows a list of included files, along with their intended use:

* **Scripts**
  * Woodcutting
    * `draynor-willows.simba` - chop and bank willow logs at Draynor

*more to follow...*
